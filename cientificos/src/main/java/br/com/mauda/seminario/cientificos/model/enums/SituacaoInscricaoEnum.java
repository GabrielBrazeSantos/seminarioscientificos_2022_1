package br.com.mauda.seminario.cientificos.model.enums;

public enum SituacaoInscricaoEnum {

    DISPONIVEL(1, "DISPONIVEL"),
    COMPRADO(2, "COMPRADO"),
    CHECKIN(3, "CHECKIN");

    private int id;
    private String nome;

    public int getId() {
        return this.id;
    }

    SituacaoInscricaoEnum(int id, String nome) {
        this.id = id;
        this.nome = nome;
    }

}